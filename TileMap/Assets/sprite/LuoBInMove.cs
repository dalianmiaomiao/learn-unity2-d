using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LuoBInMove : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator animator;
    public float speed = 2;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float Vertical = Input.GetAxis("Vertical");
        if (horizontal > 0)
        {
            Vector3 endPos = transform.position + Vector3.right * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
            animator.SetInteger("walk_state", 1);
        }
        else if (horizontal < 0) {
            Vector3 endPos = transform.position + Vector3.left * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
            animator.SetInteger("walk_state", 4);
        }
        if (Vertical > 0)
        {
            Vector3 endPos = transform.position + Vector3.up * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
            animator.SetInteger("walk_state", 2);
        }
        else if (Vertical < 0) {
            Vector3 endPos = transform.position + Vector3.down * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
            animator.SetInteger("walk_state", 3);
        }
        if (horizontal == 0 && Vertical == 0)
        {
            animator.SetInteger("walk_state", 0);
        }
    }
}
