using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class popTest : MonoBehaviour
{
    // Start is called before the first frame update
    Animator animator;
    Rigidbody2D rigidbody2D;
    public int speed = 3;
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float hor = Input.GetAxis("Horizontal");
        if (hor != 0)
        {
            Vector2 pos = rigidbody2D.position;
            rigidbody2D.MovePosition(Vector2.Lerp(pos,pos*hor *speed,Time.deltaTime));
        }

    }
}
