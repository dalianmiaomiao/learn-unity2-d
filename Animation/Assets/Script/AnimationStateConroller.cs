using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationStateConroller : MonoBehaviour
{
    Vector2 movement;
    Animator animator;
    string animatorParName = "AnimationState";

    enum State { 
        STAND = 0,
        RUN_BACK,
        RUN_RIGHT,
        RUN_UP,
        RUN_LEFT
    }
    State state;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (movement.x > 0)
        {
            state = State.RUN_RIGHT;
        }else if (movement.x < 0)
        {
            state = State.RUN_LEFT;
        }else if (movement.y < 0)
        {
            state = State.RUN_BACK;
        }else if (movement.y > 0)
        {
            state = State.RUN_UP;
        }else {
            state = State.STAND;
        }
        animator.SetInteger(animatorParName,(int)state);
    }

    private void FixedUpdate()
    {
        movement.x =  Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
    }
}
