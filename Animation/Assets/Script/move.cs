using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    // Start is called before the first frame update
    public int speed = 3;
    public GameObject Bullet;
    public Rigidbody2D rigidbody;
    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.LeftArrow)) {
            Vector3 endPos =  rigidbody.position + Vector2.left * speed;
            rigidbody.MovePosition(Vector2.Lerp(transform.position, endPos, Time.deltaTime));
        }
        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 endPos = rigidbody.position + Vector2.right * speed;
            rigidbody.MovePosition(Vector2.Lerp(transform.position, endPos, Time.deltaTime));
        }

        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Vector3 endPos = rigidbody.position + Vector2.up * speed;
            rigidbody.MovePosition(Vector2.Lerp(transform.position, endPos, Time.deltaTime));
        }

        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.DownArrow))
        {
            Vector3 endPos = rigidbody.position + Vector2.down * speed;
            rigidbody.MovePosition(Vector2.Lerp(transform.position, endPos, Time.deltaTime));
        }
    }
}
