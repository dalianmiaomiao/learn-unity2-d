using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorBike : MonoBehaviour
{
    // Start is called before the first frame update
    public Transform camer;
    public WheelJoint2D wheelLeft, wheelRight;
    public Rigidbody2D rig;
    void Start()
    {
        Vector3 pos = transform.position;
        pos.z = -10;
        camer.position = pos;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 pos = transform.position;
        pos.z = -10;
        camer.position = pos;
        JointMotor2D motor = wheelLeft.motor;
        motor.motorSpeed = -900 * Input.GetAxis("Horizontal");
        wheelLeft.motor = motor;
        wheelRight.motor = motor;
        int force = 3;
        Vector2 v2;
        v2.x = 0;
        v2.y = force* Input.GetAxis("Vertical");
        rig.AddForce(v2);

    }
}
