using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputTest : MonoBehaviour
{
    public int speed = 3; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float hAx = Input.GetAxis("Horizontal");
        if (hAx > 0)
        {
            Vector3 endPos = transform.position + Vector3.right * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
        }else if (hAx < 0) {
            Vector3 endPos = transform.position + Vector3.left * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);

        }

        if (Input.GetKey("up"))
        {
            Debug.Log("up arrow key is held down");
        }

        if (Input.GetKey("down"))
        {
            Debug.Log("down arrow key is held down");
        }

    }
}
