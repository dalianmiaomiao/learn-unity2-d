using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{
    // Start is called before the first frame update
    public int speed = 3;
    public GameObject Bullet;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.LeftArrow)) {
            Vector3 endPos = transform.position + Vector3.left * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
        }
        //键盘按下时调整精灵的位置
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 endPos = transform.position + Vector3.right * speed;
            transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
        }
        //按下空格发射子弹
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bulletInstance = Instantiate(Bullet, transform.position + Vector3.up * 1, Quaternion.identity);
        }
    }
}
