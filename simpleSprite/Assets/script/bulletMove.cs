using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bulletMove : MonoBehaviour
{
    // Start is called before the first frame update
    public int speed = 1;
    //精灵移动的最大距离
    public int maxDetection = 100;
    //精灵的起始位置，因为只向上移动，所以只记录Y轴的值
    private float startPos = 0;
    void Start()
    {
        startPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //使精灵在场景中一直向上移动
        transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.up * speed, Time.deltaTime);
        //检测运行距离
        detectionDistance();
    }

    //检测精灵移动距离，距离超过设置距离后消失
    void detectionDistance() {
        if (transform.position.y - startPos > maxDetection)
            Destroy(gameObject);
    }
}
