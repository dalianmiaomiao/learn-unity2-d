using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New RoleInformation",menuName = "RoleInformation",order = 0)]
public class RoleInformation : ScriptableObject
{
    public string roleName = "AMiao";
    public int level = 1;
}
